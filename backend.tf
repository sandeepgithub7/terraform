terraform {
  backend "s3" {
    bucket = "soma-awsbucket"
    key    = "state"
    region = "us-east-1"
    dynamodb_table = "tftable"
  }
}
